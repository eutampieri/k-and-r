from glob import glob
from sys import argv
from pygments import highlight
from pygments.lexers.c_cpp import CLexer
from pygments.formatters import HtmlFormatter

colorize = argv[2] == "True"

if colorize:
    # Save CSS
    css_path = argv[1] + "/EPUB/style/style.css"
    #original_css = open(css_path).read()
    with open(css_path, 'a') as f:
        f.write(HtmlFormatter().get_style_defs('.highlight'))

directories = glob("code/*")
chapters = [i.replace("code/", "") for i in directories]

for ch in chapters:
    if ch[:2] == "ch":
        chap_num = int(ch[-1]) + 1
        xhtml_name = argv[1] + "/EPUB/chap_" + str(chap_num).zfill(4) + ".xhtml"
        original_text = open(xhtml_name).read()
        for source in glob("code/" + ch + "/*.c"):
            print('Inserting code for "'+"$INCLUDE$" + source.split("/")[-1]+"$\" in " + xhtml_name + "...")
            code = open(source).read()
            if colorize:
                code = highlight(code, CLexer(), HtmlFormatter())
            else:
                code = "<pre>" + code.replace('&', "&amp;").replace('<', "&lt;").replace('>', "&gt;") + "</pre>"
            original_text = original_text.replace("$INCLUDE$" + source.split("/")[-1]+"$", code)
        with open(xhtml_name, 'w') as f:
            f.write(original_text)
