<package xmlns="http://www.idpf.org/2007/opf" unique-identifier="id" version="2.0" prefix="calibre: https://calibre-ebook.com">
  <metadata xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:opf="http://www.idpf.org/2007/opf">
    <dc:title id="id-1">The C programming language</dc:title>
    <dc:creator id="id-2">Brian W. Kernighan</dc:creator>
    <dc:identifier>acs6:cprogramminglang00bria:pdf:e4df7575-2bed-4581-bba4-a6250490efc4</dc:identifier>
    <dc:identifier>lccn:88005934</dc:identifier>
    <dc:identifier>oclc:254455874</dc:identifier>
    <dc:identifier>ark:ark:/13960/t8gf1wd29</dc:identifier>
    <dc:identifier>access url:http://www.archive.org/details/cprogramminglang00bria</dc:identifier>
    <dc:identifier>isbn:9780131103627</dc:identifier>
    <dc:identifier>calibre:40</dc:identifier>
    <dc:identifier>uuid:25e86832-932d-4899-979c-c5783ff40ae5</dc:identifier>
    <dc:identifier id="id">cprogramminglang00bria</dc:identifier>
    <dc:language>en</dc:language>
    <dc:date>1988-12-15T00:00:00+00:00</dc:date>
    <dc:publisher>Prentice Hall</dc:publisher>
    <dc:subject>C (Computer program language)</dc:subject>
    <opf:meta refines="#id-1" property="title-type">main</opf:meta>
    <opf:meta refines="#id-1" property="file-as">The C programming language</opf:meta>
    <meta property="dcterms:modified">2020-12-06T09:49:02Z</meta>
    <meta content="Ebook-lib 0.15.0" name="generator"/>
    <meta name="cover" content="cover-img"/>
    <meta property="schema:accessibilitySummary">The publication was generated using automated character recognition, therefore it may not be an accurate rendition of the original text, and it may not offer the correct reading sequence.This publication is missing meaningful alternative text.The publication otherwise meets WCAG 2.0 Level A.</meta>
    <meta property="schema:accessibilityFeature">printPageNumbers</meta>
    <meta property="schema:accessibilityFeature">tableOfContents</meta>
    <meta property="schema:accessMode">visual</meta>
    <meta property="schema:accessMode">textual</meta>
    <meta property="schema:accessModeSufficient">textual,visual</meta>
    <meta property="schema:accessibilityHazard">noFlashingHazard</meta>
    <meta property="schema:accessibilityHazard">noMotionSimulationHazard</meta>
    <meta property="schema:accessibilityHazard">noSoundHazard</meta>
    <meta property="schema:accessibilityControl">fullKeyboardControl</meta>
    <meta property="schema:accessibilityControl">fullMouseControl</meta>
    <meta property="schema:accessibilityControl">fullSwitchControl</meta>
    <meta property="schema:accessibilityControl">fullTouchControl</meta>
    <meta property="schema:accessibilityControl">fullVoiceControl</meta>
    <opf:meta property="role" refines="#id-2" scheme="marc:relators">aut</opf:meta>
    <opf:meta property="file-as" refines="#id-2">Kernighan, Brian W.</opf:meta>
    <opf:meta property="calibre:author_link_map">{"Brian W. Kernighan": ""}</opf:meta>
  </metadata>
  <manifest>
    <item href="cover.xhtml" id="cover" media-type="application/xhtml+xml" properties="calibre:title-page"/>
    <item href="nav.xhtml" id="nav" media-type="application/xhtml+xml" properties="nav"/>
    <item href="chap_0000.xhtml" id="chapter_2" media-type="application/xhtml+xml"/>
    <item href="chap_0001.xhtml" id="chapter_3" media-type="application/xhtml+xml"/>
    <item href="chap_0002.xhtml" id="id1" media-type="application/xhtml+xml"/>
    <item href="chap_0003.xhtml" id="id2" media-type="application/xhtml+xml"/>
    <item href="chap_0004.xhtml" id="id3" media-type="application/xhtml+xml"/>
    <item href="chap_0005.xhtml" id="id4" media-type="application/xhtml+xml"/>
    <item href="chap_0006.xhtml" id="id5" media-type="application/xhtml+xml"/>
    <item href="chap_0007.xhtml" id="id6" media-type="application/xhtml+xml"/>
    <item href="chap_0008.xhtml" id="id7" media-type="application/xhtml+xml"/>
    <item href="chap_0009.xhtml" id="id8" media-type="application/xhtml+xml"/>
    <item href="chap_0010.xhtml" id="id9" media-type="application/xhtml+xml"/>
    <item href="chap_0011.xhtml" id="id10" media-type="application/xhtml+xml"/>
    <item href="chap_0012.xhtml" id="chapter_4" media-type="application/xhtml+xml"/>
    <item href="toc.ncx" id="ncx" media-type="application/x-dtbncx+xml"/>
    <item href="style/style.css" id="style_nav" media-type="text/css"/>
    <item href="images/cover.jpg" id="cover-img" media-type="image/jpeg" properties="cover-image"/>
    <item href="images/p101-1.png" id="id15" media-type="image/png"/>
    <item href="images/p101-2.png" id="id16" media-type="image/png"/>
    <item href="images/p104.png" id="id17" media-type="image/png"/>
    <item href="images/p107.png" id="id18" media-type="image/png"/>
    <item href="images/p114-1.png" id="id19" media-type="image/png"/>
    <item href="images/p114-2.png" id="id20" media-type="image/png"/>
    <item href="images/p115-1.png" id="id21" media-type="image/png"/>
    <item href="images/p123.png" id="id22" media-type="image/png"/>
    <item href="images/p127.png" id="id23" media-type="image/png"/>
    <item href="images/p129.png" id="id24" media-type="image/png"/>
    <item href="images/p139.png" id="id25" media-type="image/png"/>
    <item href="images/p144.png" id="id26" media-type="image/png"/>
    <item href="images/p185.jpg" id="id27" media-type="image/jpeg"/>
    <item href="images/p186.png" id="id28" media-type="image/png"/>
    <item href="images/p96.png" id="id11" media-type="image/png"/>
    <item href="images/p98-1.png" id="id12" media-type="image/png"/>
    <item href="images/p98-2.png" id="id13" media-type="image/png"/>
    <item href="images/p98-3.png" id="id14" media-type="image/png"/>
  </manifest>
  <spine page-progression-direction="ltr" toc="ncx">
    <itemref idref="cover"/>
    <itemref idref="nav"/>
    <itemref idref="chapter_2"/>
    <itemref idref="chapter_3"/>
    <itemref idref="id1"/>
    <itemref idref="id2"/>
    <itemref idref="id3"/>
    <itemref idref="id4"/>
    <itemref idref="id5"/>
    <itemref idref="id6"/>
    <itemref idref="id7"/>
    <itemref idref="id8"/>
    <itemref idref="id9"/>
    <itemref idref="id10"/>
    <itemref idref="chapter_4"/>
  </spine>
</package>